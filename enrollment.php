<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>LHCS Student Portal</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/lchs.png" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container-xxl position-relative bg-white d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Sidebar Start -->
        <?php include "sidebar.php"; ?>
        <!-- Sidebar End -->


        <!-- Content Start -->
        <div class="content">
            <!-- Navbar Start -->
            <?php include "topbar.php" ?>
            <!-- Navbar End -->


            <!-- Blank Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="row vh-100 bg-light rounded align-items-center justify-content-center mx-0" style="display:none" id="enrolled_div">
                    <div class="col-md-6 text-center">
                        <h3>You are already enrolled</h3>
                        <h5>This page will be available on the next school year</h5>
                        <h7>Please feel free to contact the admin office at 123-456 for any concern about your enrollment</h7>
                    </div>
                </div>
                <div class="row vh-100 bg-light rounded align-items-center justify-content-center mx-0" style="display:none" id="finished">
                    <div class="col-md-6 text-center">
                        <h3>You already finished the Basic Education</h3>
                        <h5>Please settle your balances if there are to be able to produce your necessary documents</h5>
                        <h7>Please feel free to contact the admin office at 123-456 for any concern about this</h7>
                        <h7>Thanks for choosing LHCS !</h7>
                    </div>
                </div>
                <div class="row g-4" id="not_enrolled_div">
                    <div class="rounded h-100 p-4">
                        <h5 class="mb-12" id="school_year_detail"></h5>
                        <h6 class="mb-4">Student Enrollment Information</h6>
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                                        Basic Information
                                    </button>
                                </h2>
                                <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <form id="student_information">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">First Name</label>
                                                        <input type="text" readonly class="form-control form-control-sm" name="fname">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Middle Name</label>
                                                        <input type="text" readonly class="form-control form-control-sm" name="mname">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Last Name</label>
                                                        <input type="text" readonly class="form-control form-control-sm" name="lname">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputPassword1" class="form-label">Grade Level</label>
                                                        <!-- <select class="form-select form-select-sm" aria-label=".form-select-sm example" name="grade_level">
                                                            <option>Kinder 1</option>
                                                            <option>Kinder 2</option>
                                                            <option>Grade 1</option>
                                                            <option>Grade 2</option>
                                                            <option>Grade 3</option>
                                                            <option>Grade 4</option>
                                                            <option>Grade 5</option>
                                                            <option>Grade 6</option>
                                                        </select> -->
                                                        <input type="text" readonly class="form-control form-control-sm" name="grade_level" id="grade_level">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="mb-3">
                                                        <label for="exampleInputPassword1" class="form-label">Birthdate</label>
                                                        <input type="lrn" class="form-control form-control-sm" id="exampleInputPassword1" name="birthday">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Address</label>
                                                        <input type="text" class="form-control form-control-sm" id="exampleInputEmail1" name="address">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputPassword1" class="form-label">LRN</label>
                                                        <input type="lrn" class="form-control form-control-sm" id="exampleInputPassword1" name="lrn">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Previous School</label>
                                                        <input type="text" class="form-control form-control-sm" id="exampleInputEmail1" name="prev_school">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">

                                                    
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Place of Birth</label>
                                                        <input type="text" class="form-control form-control-sm" id="exampleInputEmail1" name="place_of_birth">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Father's Name</label>
                                                        <input type="text" class="form-control form-control-sm" id="exampleInputEmail1" name="fathers_name">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Mother's Name</label>
                                                        <input type="text" class="form-control form-control-sm" id="exampleInputEmail1" name="mothers_name">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Name of Guardian</label>
                                                        <input type="text" class="form-control form-control-sm" id="exampleInputEmail1" name="name_of_guardian">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Relationship to Student</label>
                                                        <input type="text" class="form-control form-control-sm" id="exampleInputEmail1" name="relationship_student">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="exampleInputEmail1" class="form-label">Contact Number</label>
                                                        <input type="number" class="form-control form-control-sm" id="exampleInputEmail1" name="contact_number">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                        Documents
                                    </button>
                                </h2>
                                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <div class="col-sm-12 col-xl-6">
                                            <div class="bg-light rounded h-100 p-4">
                                                <h6 class="mb-4">Upload the required documents</h6>
                                                <div class="mb-3">
                                                    <label for="formFile" class="form-label">2x2 Picture</label>
                                                    <input class="form-control" type="file" id="image_2x2">
                                                </div>
                                                <div class="mb-3">
                                                    <label for="formFileMultiple" class="form-label">Birth Certificate</label>
                                                    <input class="form-control" type="file" id="birth_certificate">
                                                </div>

                                                <div class="mb-3">
                                                    <label for="formFileMultiple" class="form-label">Report of Grades</label>
                                                    <input class="form-control" type="file" id="report_grades">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                        Payment Options
                                    </button>
                                </h2>
                                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <br />
                                                <h6 class="mb-4">Payment Options</h6>
                                                <div class="btn-group-vertical" role="group">

                                                    <input type="radio" class="btn-check" value="CASH" name="btnradio" id="btnradio1" autocomplete="off" checked>
                                                    <label class="btn btn-outline-primary" for="btnradio1">Cash</label>

                                                    <input type="radio" class="btn-check" value="SEMESTRAL" name="btnradio" id="btnradio2" autocomplete="off">
                                                    <label class="btn btn-outline-primary" for="btnradio2">Semestral</label>

                                                    <input type="radio" class="btn-check" value="QUARTERLY" name="btnradio" id="btnradio3" autocomplete="off">
                                                    <label class="btn btn-outline-primary" for="btnradio3">Quarterly</label>

                                                    <input type="radio" class="btn-check" value="MONTHLY" name="btnradio" id="btnradio4" autocomplete="off">
                                                    <label class="btn btn-outline-primary" for="btnradio4">Monthly</label>

                                                    <input type="radio" class="btn-check" value="ESPAY" name="btnradio" id="btnradio5" autocomplete="off">
                                                    <label class="btn btn-outline-primary" for="btnradio5">Easy Pay</label>

                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="rounded h-100 p-4">
                                                    <h6 class="mb-4">Balance Breakdown</h6>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Bill Date</th>
                                                                    <th scope="col">Description</th>
                                                                    <th scope="col">Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="paymentBreakdown">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="alert alert-info" role="alert" id="wait" style="display:none">
                                                        Please wait...
                                                    </div>

                                                    <div class="alert alert-primary" role="alert" id="successful" style="display:none">
                                                        Successfully Submitted Evaluation Form Please wait for the Approval
                                                    </div>

                                                    <div class="alert alert-danger" role="alert" id="error" style="display:none">
                                                        You already submitted the evaluation please contact the administrator
                                                    </div>
                                                    <button type="button" onclick="saveEnrollmentForm()" class="btn btn-info m-2" style="float: right;">Submit Enrollment</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Blank End -->


            <!-- Footer Start -->
            <?php include "footer.php" ?>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/chart/chart.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/tempusdominus/js/moment.min.js"></script>
    <script src="lib/tempusdominus/js/moment-timezone.min.js"></script>
    <script src="lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
    <script src="js/formhelper.js"></script>
    <script src="js/enrollment.js"></script>
</body>

</html>