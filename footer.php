<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded-top p-4">
        <div class="row">
            <div class="col-12 col-sm-6 text-center text-sm-start">
                 <a href="#">Light in the Haus Christian School</a>
            </div>
            <div class="col-12 col-sm-6 text-center text-sm-end">
                &copy;All Right Reserved. 2022
            </div>
        </div>
    </div>
</div>