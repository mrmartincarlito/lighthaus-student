<div class="sidebar pe-4 pb-3">
            <nav class="navbar bg-light navbar-light">
                <a href="index.html" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary"><i class="fa fa-hashtag me-2"></i>LHCS PORTAL</h3>
                </a>
                <div class="d-flex align-items-center ms-4 mb-4">
                    <div class="position-relative">
                        <img class="rounded-circle" src="img/lchs.png" alt="" style="width: 40px; height: 40px;">
                        <div class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
                    </div>
                    <div class="ms-3">
                        <h6 class="mb-0" id="student_name"></h6>
                        <span id="student_grade_level_display"></span>
                    </div>
                </div>
                <div class="navbar-nav w-100">
                    <a href="dashboard.php" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
                    <a href="enrollment.php" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Enrollment</a>
                    <a href="grade.php" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Grade</a>
                    <a href="bills-payment.php" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Bills Payment</a>
                </div>
            </nav>
        </div>