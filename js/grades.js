const GRADES_API = LIGHTHAUS_BACKOFFICE_API + 'api/student.grade.php';

getStudentGrade();
function getStudentGrade() {
    $.ajax({
		url: GRADES_API,
		type: 'get',
		data: '_getStudentGrade',
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)
            
            var tr1 = ""

            for (var i=0; i < responseJSON.values.length; i++) {
                var details = responseJSON.values[i];
                
                $("#name").html(details.student_name)
                $("#grade_section").html(responseJSON.detail.grade_level + " / " + details.section)
                $("#school_year").html(details.school_year)

                tr1 = tr1 + `
                    <tr>
                        <td>` + (i+1) + `</td>
                        <td>` +details.code+ `</td>
                        <td>` +details.description+ `</td>
                        <td>` +details.faculty_name+ `</td>
                        <td style="text-align:right">` +details.first+ `</td>
                        <td style="text-align:right">` +details.second+ `</td>
                        <td style="text-align:right">` +details.third+ `</td>
                        <td style="text-align:right">` +details.fourth+ `</td>
                        <td style="text-align:right">` +details.final+ `</td>
                        <td style="text-align:right">` +details.remarks+ `</td>
                    </tr>
                `
            }

            if (tr1 == "") {
                tr1 = `
                    <tr>
                        <td colspan="10" style="text-align:center">No Record Found</td>
                    </tr>
                `
            }


            $("#student_grade").html(tr1)

			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}