const FORGOT_API = LIGHTHAUS_BACKOFFICE_API + 'api/forgot-password.php';

$('form').submit(function(e) {
	var data = $(this).serializeArray()
	var params = postParams('login', data)

	$("#wait").show()

	login(params);

	e.preventDefault()
})

function login(params) {
	$.ajax({
		url: LOGIN_API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {

			try {
				responseJSON = $.parseJSON(data)

				if (responseJSON.type == 'success') {
					$("#successful").show()
					$("#error").hide()
	
					localStorage.setItem("logged-user", responseJSON.info)
					localStorage.setItem("logged-student", responseJSON.student)
	
					window.location.href = 'dashboard.php'
				} else {
					$("#successful").hide()
					$("#error").show()
				}
	
				$("#wait").hide()
			} catch (err) {
				login(params);
			}

		})
		.fail(errorThrown => {
			console.log('Login POST Response: ', errorThrown)
			console.log(errorThrown)
			return false
		})
}

function forgotPassword() {
	let username = $("#floatingInput").val();

	if (username == "") {
		alert ("Please enter your username");
		return;
	}

	$.ajax({
		url: FORGOT_API,
		type: 'post',
		data: 'forgot=1&username=' + username,
		processData: false
	})
		.done(data => {
			console.log(data)
			$("#success").hide()
			$("#error").hide()
			$("#forgot").show()
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
	
}
