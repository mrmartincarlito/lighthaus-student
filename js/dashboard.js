const DASHBOARD_API = LIGHTHAUS_BACKOFFICE_API + 'api/student.dashboard.php';
const IMAGE = LIGHTHAUS_BACKOFFICE_API + 'api/student.upload_image.receipt.php'

getDashboard();

var balanceDue = null;

function getDashboard() {
    $.ajax({
		url: DASHBOARD_API,
		type: 'get',
		data: '_getDashboard',
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)
            $.each(responseJSON[0].cards, function (i, item) {
                if (i == "status" && (item == "" || item == null)) {
                    item = "NOT ENROLLED"
                }

				$("#" + i).html(item)
			});

            var tr = "";
            balanceDue = responseJSON[0].balance_due_table;

            for (var i = 0; i<responseJSON[0].balance_due_table.length; i++) {

                var records = responseJSON[0].balance_due_table[i];

                tr = tr +
                `
                    <tr>
                        <td><input type="checkbox" value="` +records.id + `" name="paymentSelected"></td>
                        <td>` +records.billed_date+ `</td>
                        <td>` +records.billed_amount+ `</td>
                        <td class="bg-danger text-white"><center>` +records.status+ `</center></td>
                    </tr>
                `;
            }

            if (tr == "") {
                tr = `
                    <tr>
                        <td colspan="4" style="text-align:center">No Record Found</td>
                    </tr>
                `
            }

            $("#balanceTbody").html(tr)

            var subjectSched = "";

            for (const element of responseJSON[0].subjects) {
                var records = element;
                var date = new Date();
                var today = date.getFullYear() + "-" + String(date.getMonth() + 1).padStart(2, '0') + "-" + String(date.getDate()).padStart(2, '0');
                
                if (today == records.start.split(" ")[0]) {
                    
                    subjectSched = subjectSched + `
                        <div class="d-flex align-items-center border-bottom py-2" >
                            <div class="w-100 ms-3" >
                                <div class="d-flex w-100 align-items-center justify-content-between">
                                    <span>` +records.title+ `</span>
                                    <span>` +records.start.split(" ")[1]+ ` - `+records.end.split(" ")[1]+`</span>
                                </div>
                            </div>
                        </div>
                    `
                }

            }

            if (subjectSched == "") {
                subjectSched = `
                    <div class="d-flex align-items-center border-bottom py-2" >
                        <div class="w-100 ms-3" >
                            <div class="d-flex w-100 align-items-center justify-content-between">
                                <span>No Subject Schedule</span>
                            </div>
                        </div>
                    </div>
                `
            }

            $("#subject_schedule").html(subjectSched)


            var announcements = "";

            for (const element of responseJSON[0].announcements) {
                var records = element;
                var imageContent = "";

                if (records.image_file != "") {
                    imageContent = "<br /><br /><span><img width='100%' src='" + LIGHTHAUS_BACKOFFICE_API + 'api/'+ records.image_file+ "'></span>";
                }

                announcements = announcements + `
                    <br />
                    <div class="d-flex align-items-center border-bottom py-2" >
                        <div class="w-100 ms-3" >
                            <div class="align-items-center">
                                <span><strong>` +records.description+ `</strong></span> 
                                ${imageContent}
                            </div>
                        </div>
                    </div>
                `
            }

            if (announcements == "") {
                announcements = `
                    <div class="d-flex align-items-center border-bottom py-2" >
                        <div class="w-100 ms-3" >
                            <div class="d-flex w-100 align-items-center justify-content-between">
                                <span>No Announcements</span>
                            </div>
                        </div>
                    </div>
                `
            }

            $("#announcements").html(announcements)

			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

function paySelectedPayment() {
    var paymentSelected = []
    $('input[name=paymentSelected]').each(function () {
        if (this.checked)
            paymentSelected.push($(this).val());
    });

    if (paymentSelected.length == 0) {
        alert("No Payment Selected")
        return;
    }

    $("#payment_div").show()

    var totalAmountSelected = 0;

    for (var i = 0; i<balanceDue.length; i++) {
        for (var j = 0; j<paymentSelected.length; j++) {
            if (balanceDue[i].id == paymentSelected[j]) {
                totalAmountSelected = totalAmountSelected + parseFloat(balanceDue[i].billed_amount)
            }
        }
    }

    $("#total_amount_selected").html("Php "+ totalAmountSelected.toFixed(2))
}

function submitPayment () {

    if (!confirm("Are you sure you want to submit this payment ?")) {
        return;
    }

    if ( !$('#image_file').val() ) {
        alert("No Image Receipt selected")
        return;
    }

    var paymentSelected = []
    $('input[name=paymentSelected]').each(function () {
        if (this.checked)
            paymentSelected.push($(this).val());
    });

    if (paymentSelected.length == 0) {
        alert("No Payment Selected")
        return;
    }

    $.ajax({
		url: DASHBOARD_API,
		type: 'post',
		data: '_payBalanceDue=' + JSON.stringify({payments : paymentSelected}),
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)
            
            if (responseJSON.type == "success") {
                alert("Payment has been made please wait for the approval")
                saveImage();
            } else {
                alert ("Something went wrong please try again")
            }
            
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

function saveImage() {
	var formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);

	$.ajax({
		url: IMAGE,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		console.log(data)
		response = JSON.parse(data)
        window.location.reload()
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}