const PROFILE_API = LIGHTHAUS_BACKOFFICE_API + 'api/student.profile.php';


function saveChanges () {

    if (!confirm("Are you sure you want to change your password ?")) {
        return;
    }

    var request = {
        oldPassword : $("#oldPassword").val(),
        newPassword : $("#newPassword").val(),
        confirmPassword : $("#confirmPassword").val()
    };

    $.ajax({
		url: PROFILE_API,
		type: 'post',
		data: '_setEditAccount=' + JSON.stringify(request),
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)

            if (responseJSON.type == 'success') {
                $("#success").show()
                $("#success_msg").html(responseJSON.text)

                $("#error").hide()
            } else {
                $("#error").show()
                $("#error_msg").html(responseJSON.text)

                $("#success").hide()
            }
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}