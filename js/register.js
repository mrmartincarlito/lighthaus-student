const REGISTRATION_API = LIGHTHAUS_BACKOFFICE_API + 'api/register.php';

$('form').on('submit', function (e) {
	if (!confirm('Are you sure you want to register?')) {
		return false
	}

	e.preventDefault()

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: REGISTRATION_API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)

			if (responseJSON.type == 'success') {
                $("#successful").show()
                $("#error").hide()
				$('form').trigger("reset")
            } else {
                $("#successful").hide()
                $("#error").show()
            }
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
})
