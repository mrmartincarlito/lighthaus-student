const ENROLLMENT_API = LIGHTHAUS_BACKOFFICE_API + 'api/student.enrollment.php';
const IMAGE_2X2 = LIGHTHAUS_BACKOFFICE_API + 'api/student.upload_image.2x2.php';
const BIRTHCERTIFICATE = LIGHTHAUS_BACKOFFICE_API + 'api/student.upload_image.birthcertificate.php';
const REPORT_GRADES = LIGHTHAUS_BACKOFFICE_API + 'api/student.upload_image.report_grades.php';

getStudentInformation();
function getStudentInformation() {
    $.ajax({
		url: ENROLLMENT_API,
		type: 'get',
		data: '_getStudentDetails',
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)
            populateForm($("#student_information"), responseJSON.student[0])

            $("#school_year_detail").html(responseJSON.open_school_year.description)

            let currentGrade = responseJSON.student[0].grade_level;

            if (responseJSON.current_school_year && responseJSON.current_school_year.status == "ENROLLED") {
                $("#enrolled_div").show()
                $("#not_enrolled_div").hide()
            } else {
                $("#enrolled_div").hide()
                
                if (responseJSON.enrollment_details[currentGrade] != null) {
                    if (responseJSON.enrollment_details[currentGrade].school_year_status === "COMPLETED") {
                        if (currentGrade == "Kinder 1"  ) {
                            $("#grade_level").val("Kinder 2");
                        }
        
                        else if (currentGrade == "Kinder 2"  ) {
                            $("#grade_level").val("Grade 1");
                        }
        
                        else if (currentGrade == "Grade 1"  ) {
                            $("#grade_level").val("Grade 2");
                        }
        
                        else if (currentGrade == "Grade 2"  ) {
                            $("#grade_level").val("Grade 3");
                        }
        
                        else if (currentGrade == "Grade 3"  ) {
                            $("#grade_level").val("Grade 4");
                        }
        
                        else if (currentGrade == "Grade 4"  ) {
                            $("#grade_level").val("Grade 5");
                        }
        
                        else if (currentGrade == "Grade 6"  ) {
                            $("#finished").show()
                        }
        
                        else {
                            $("#not_enrolled_div").show()
                        }
                    } else {
                        $("#not_enrolled_div").show()
                    }
                } else {
                    $("#not_enrolled_div").show()
                }
                
            }
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

var paymentDetails = null;

getEnrollmentPayments()
function getEnrollmentPayments () {
    $.ajax({
		url: ENROLLMENT_API,
		type: 'get',
		data: '_getEnrollmentPayment',
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)
            paymentDetails = responseJSON
            

            var tr = ""

            tr = tr + `
                <tr>
                    <td>1</td>
                    <td>Date</td>
                    <td>Overall Payment</td>
                    <td>` +paymentDetails.overAllPayment.cash+ `</td>
                </tr>
            `

            $("#paymentBreakdown").html(tr)
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

$('input:radio[name=btnradio]').change(function () {
    var selected = $("input[name='btnradio']:checked").val()
    
    var tr = ""

    if (selected == "CASH") {
        tr = tr + `
                <tr>
                    <td>1</td>
                    <td>Upon Enrollment</td>
                    <td>Overall Payment</td>
                    <td>` +paymentDetails.overAllPayment.cash+ `</td>
                </tr>
            `
    }
    const d = new Date();
    let year = d.getFullYear();
    let nextYear = year + 1;
    var scheds = [
        "05-09-" + year,
        "05-10-" + year,
        "05-11-" + year,
        "05-12-" + year,
        "05-01-" + nextYear,
        "05-02-"  + nextYear,
        "05-03-" + nextYear,
        "05-04-" + nextYear,
        "05-05-"  + nextYear
    ]

    if (selected == "SEMESTRAL") {
        tr = tr + `
        <tr>
            <td>1</td>
            <td>Upon Enrollment</td>
            <td>Down Payment</td>
            <td>` +paymentDetails.initialDownPayment[0].semestral+ `</td>
        </tr>
    `
        for(var i = 0; i<scheds.length; i++) {

            var payment = 0;
            $.each(responseJSON.breakDown[0].semestral, function (key, item) {
                if (key == scheds[i]) {
                    payment = item
                }
			});

            tr = tr + `
                <tr>
                    <td>` +(i+2)+ `</td>
                    <td>` +scheds[i]+ `</td>
                    <td>Installment</td>
                    <td>` +parseFloat(payment).toFixed(2)+ `</td>
                </tr>
            `
        }
        
    }

    if (selected == "QUARTERLY") {
        tr = tr + `
        <tr>
            <td>1</td>
            <td>Upon Enrollment</td>
            <td>Down Payment</td>
            <td>` +paymentDetails.initialDownPayment[0].quarterly+ `</td>
        </tr>
    `

        for(var i = 0; i<scheds.length; i++) {

            var payment = 0;
            $.each(responseJSON.breakDown[1].quarterly, function (key, item) {
                if (key == scheds[i]) {
                    payment = item
                }
            });

            tr = tr + `
                <tr>
                    <td>` +(i+2)+ `</td>
                    <td>` +scheds[i]+ `</td>
                    <td>Installment</td>
                    <td>` +parseFloat(payment).toFixed(2)+ `</td>
                </tr>
            `
        }
    }

    if (selected == "MONTHLY") {
        tr = tr + `
        <tr>
            <td>1</td>
            <td>Upon Enrollment</td>
            <td>Down Payment</td>
            <td>` +paymentDetails.initialDownPayment[0].monthly+ `</td>
        </tr>
    `
        for(var i = 0; i<scheds.length; i++) {

            var payment = 0;
            $.each(responseJSON.breakDown[2].monthly, function (key, item) {
                if (key == scheds[i]) {
                    payment = item
                }
            });

            tr = tr + `
                <tr>
                    <td>` +(i+2)+ `</td>
                    <td>` +scheds[i]+ `</td>
                    <td>Installment</td>
                    <td>` +parseFloat(payment).toFixed(2)+ `</td>
                </tr>
            `
        }
    }

    if (selected == "ESPAY") {
        tr = tr + `
        <tr>
            <td>1</td>
            <td>Upon Enrollment</td>
            <td>Down Payment</td>
            <td>` +paymentDetails.initialDownPayment[0].espay+ `</td>
        </tr>
    `
        for(var i = 0; i<scheds.length; i++) {

            var payment = 0;
            $.each(responseJSON.breakDown[3].espay, function (key, item) {
                if (key == scheds[i]) {
                    payment = item
                }
            });

            tr = tr + `
                <tr>
                    <td>` +(i+2)+ `</td>
                    <td>` +scheds[i]+ `</td>
                    <td>Installment</td>
                    <td>` +parseFloat(payment).toFixed(2)+ `</td>
                </tr>
            `
        }
    }

    $("#paymentBreakdown").html(tr)
});

function saveEnrollmentForm() {
    if (!confirm('Are you sure you want to submit evaluation form?')) {
		return false
	}

    var data = $('form#student_information').serializeArray()
	var params = postParams('', data)
    $("#wait").show()

    $.ajax({
		url: ENROLLMENT_API,
		type: 'post',
		data: 'data=' + params + '&type=' + $("input[name='btnradio']:checked").val(),
		processData: false
	})
		.done(data => {

			responseJSON = $.parseJSON(data)
            
            if (responseJSON.type == 'success') {
                $("#successful").show()
                $("#error").hide()
                saveImage('image_2x2', IMAGE_2X2)
                saveImage('birth_certificate', BIRTHCERTIFICATE)
                saveImage('report_grades', REPORT_GRADES)
            } else {
                $("#successful").hide()
                $("#error").show()
            }

            $("#wait").hide()
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

function saveImage(inputFileId, url) {
	var formData = new FormData();
	formData.append('image_file', $('#' + inputFileId)[0].files[0]);

	$.ajax({
		url: url,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		console.log(data)
		response = JSON.parse(data)
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}