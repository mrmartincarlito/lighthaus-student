const BILLSPAYMENT_API = LIGHTHAUS_BACKOFFICE_API + 'api/student.bills-payment.php';

getBillsPaymment();
function getBillsPaymment() {
    $.ajax({
        url: BILLSPAYMENT_API,
        type: 'get',
        data: '_getBillsPayments',
        processData: false
    })
        .done(data => {

            let responseJSON = $.parseJSON(data)

            var tr1 = ""
            var counter = 1;
            for (const element of responseJSON.all_payments) {

                var className = "bg-primary";

                if (element.status == "DUE")
                    className = "bg-danger"

                if (element.status == "REJECTED")
                    className = "bg-warning"

                if (element.status == "PENDING")
                    className = "bg-success"
                
                if (element.status == "")
                    className = "bg-light"

                tr1 = tr1 + `
                    <tr>
                        <td>` + counter + `</td>
                        <td>` + element.payment_description + `</td>
                        <td>` + element.billed_date + `</td>
                        <td>` + element.billed_amount + `</td>
                        <td class='${className} text-white'><center>` + element.status + `</center></td>
                    </tr>
                `
                counter++;
            }

            if (tr1 == "") {
                tr1 = `
                    <tr>
                        <td colspan="5" style="text-align:center">No Record Found</td>
                    </tr>
                `
            }


            $("#schedule_payment").html(tr1)


            var tr2 = ""
            counter = 1;
            for (const element of responseJSON.history)  {


                var className = "bg-primary";

                if (element.status == "DUE")
                    className = "bg-danger"

                if (element.status == "REJECTED")
                    className = "bg-warning"

                if (element.status == "PENDING")
                    className = "bg-success"
                
                if (element.status == "")
                    className = "bg-light"

                tr2 = tr2 + `
                    <tr>
                        <td>` + counter + `</td>
                        <td>` + element.amount + `</td>
                        <td>` + element.date_paid + `</td>
                        <td>` + element.or_number + `</td>
                        <td>` + element.approved_by + `</td>
                        <td>` + element.remarks + `</td>
                        <td class='${className} text-white'><center>` + element.status + `</center></td>
                    </tr>
                `
                counter ++;
            }

            if (tr2 == "") {
                tr2 = `
                    <tr>
                        <td colspan="7" style="text-align:center">No Record Found</td>
                    </tr>
                `
            }

            $("#payment_history").html(tr2)

        })
        .fail(errorThrown => {
            $.unblockUI()
            console.log('Save Changes Post Error: ', errorThrown)
            return false
        })
}